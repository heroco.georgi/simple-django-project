ssh $EC2_USER@$EC2_HOST_IP << EOF
  cd ~/simple-django-project
  git fetch origin
  git pull
  docker-compose down
  docker-compose up -d
EOF